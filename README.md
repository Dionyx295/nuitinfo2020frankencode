# NuitInfo2020Frankencode

L'équipe confipote relèvent le défi de SEPTEO : Frankencode : créer la vie avec du code !

Equipe : Confipote      Site : Polytech Tours       Leader : Jean-Malo Chabin

Nous avons développé en python un simulateur du jeu de la vie.

Pour fonctionner il faut simplement les bibliothèques numpy et tkinter.

Lorsqu'on lance notre programme une fenêtre apparait avec une partie réservé au jeu de la vie.
Le premier affichage correspond à ce qu'on pourrait afficher lors d'un écran de chargement.

La partie bleu représente les cellules mortes, et vert les cellules vivantes.
On a choisi ce code couleur pour rappeler le thème de cette anné : bleu comme l'océan et vert comme la pollution qu'il contient.

Cette page est dynamique directement avec le clic. A tout moment un clic gauche ajoute une cellule vivante et un clic droit ajoute une cellule morte.

Pour facilité l'utilisation de notre programme on a ajouté certaines options de manipulation:

Bouton Go :
    lance l'annimation, si l'animation est en cours le bouton est désactivé et affiche le texte "Running"

Bouton Stop :
    met en pause l'animation et réactive le bouton Go

Bouton Reset :
    Nettoie la fenêtre en mettant toutes les cellules à l'état "morte"

Entrée "vitesse" :
    on peut saisir la durée entre deux itérations du jeu (saisir entrée pour valider). Cette durée est en milliseconde et par default à 400ms.

On a aussi ajouté certains boutons permettant de découvrir des structures

Bouton Planeur :
    supprime toutes les cellules vivantes puis fais apparaitre le structure du planeur. 5 cellules se déplacement perpetuellement.(si la fenêtre était de taille infini)

Bouton Canon :
    supprime toutes les cellules vivantes puis fais apparaitre la canon a planeur de Gosper.

Bouton Pento :
    supprime toutes les cellules vivantes puis fais apparaitre un exemple de "Mathusalem" (structure largement active)

Auteurs:
Jean-Malo Chabin,
Adrien Prevost,
Alan Guenegou,
Guillaume Guichard,
Alex Nouvelet,
Tom Wauqier,
Alaric Lecrenais,
Romain Fragnier
