from tkinter import *
import numpy as np

        
 #fonction rendant vivante la cellule cliquée donc met la valeur 1 pour la cellule cliquée 
def click_gauche(event):
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    canDamier.create_rectangle(x, y, x+c, y+c, fill='#00CD00', outline='#00CD00')
    mat_etat_courant[y//c,x//c]=1

#fonction tuant la cellule cliquée donc met la valeur 0 pour la cellule cliquée
def click_droit(event): 
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    canDamier.create_rectangle(x, y, x+c, y+c, fill='#00CECE', outline='#00CECE')
    mat_etat_courant[y//c,x//c]=0

#fonction pour changer la vitesse(l'attente entre chaque étape)
def change_vit(event): 
    global vitesse
    if int(eval(entreeVit.get()))>0:
        vitesse = int(eval(entreeVit.get()))
        print("vitesse : ",vitesse,"ms")
   
#fonction qui démarre l'animation
def go():
    global flag
    if flag ==0:
        flag =1
        bRun['text']='Running...'
        play()
  
#fonction qui met en pause l'animation
def stop():
    "arrêt de l'animation"
    global flag
    flag =0
    bRun['text']='     Go!     '
    
    #on affiche certaines statistiques dans la console
    print(compteurIteration," iterations") #On affiche le nombre d'iteration
    if compteurIteration != 0:   
        print (int(compteurCelluleActiveTotal/compteurIteration),"cellules actives par itérations")

 

#supprime (tue) toutes les cellules
def reset():
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
                mat_etat_courant[y,x]=0 
                mat_etat_suivant[y,x]=0
    redessiner()
    stop()
    
#affiche un exemple type ecran de chargement
def exemple():
    x=width_mat//2
    y=height_mat//2
    
    insert3BarX4(x,y)
    insert3Bar(x+10,y+10)
    insert3Bar(x-10,y-10)
    insert3Bar(x+10,y-10)
    insert3Bar(x-10,y+10)
    
#insert trois cellules vivantes cote à cote
def insert3Bar(x, y):
    if x>2 and y>2 and x<width_mat-2 and y<height_mat-2:
        mat_etat_courant[y,x]=1
        mat_etat_courant[y,x+1]=1
        mat_etat_courant[y,x-1]=1
        
#motif creant 4 barres de trois 
def insert3BarX4(x, y):
    if x>10 and y>10 and x<width_mat-10 and y<height_mat-10:
        mat_etat_courant[y,x]=1
        mat_etat_courant[y,x+1]=1
        mat_etat_courant[y,x-1]=1
        mat_etat_courant[y+1,x]=1

#motif du canon a planeur de Gosper
def insertCanon(x=5,y=30):
    #left square
    mat_etat_courant[y,x]=1
    mat_etat_courant[y+1,x]=1
    mat_etat_courant[y,x+1]=1
    mat_etat_courant[y+1,x+1]=1
    
    #arc
    mat_etat_courant[y,x+10]=1
    mat_etat_courant[y+1,x+10]=1
    mat_etat_courant[y-1,x+10]=1
    mat_etat_courant[y+2,x+11]=1
    mat_etat_courant[y-2,x+11]=1
    mat_etat_courant[y+3,x+12]=1
    mat_etat_courant[y+3,x+13]=1
    mat_etat_courant[y-3,x+12]=1
    mat_etat_courant[y-3,x+13]=1
    
    mat_etat_courant[y,x+14]=1
    
    mat_etat_courant[y+2,x+15]=1
    mat_etat_courant[y-2,x+15]=1
    mat_etat_courant[y+1,x+16]=1
    mat_etat_courant[y,x+16]=1
    mat_etat_courant[y-1,x+16]=1
    mat_etat_courant[y,x+17]=1
    
    mat_etat_courant[y+1,x+20]=1
    mat_etat_courant[y+2,x+20]=1
    mat_etat_courant[y+3,x+20]=1
    mat_etat_courant[y+1,x+21]=1
    mat_etat_courant[y+2,x+21]=1
    mat_etat_courant[y+3,x+21]=1
    mat_etat_courant[y+4,x+22]=1
    mat_etat_courant[y,x+22]=1
    
    mat_etat_courant[y+4,x+24]=1
    mat_etat_courant[y+5,x+24]=1
    
    mat_etat_courant[y,x+24]=1
    mat_etat_courant[y-1,x+24]=1
    
    mat_etat_courant[y+2,x+34]=1
    mat_etat_courant[y+3,x+34]=1
    mat_etat_courant[y+2,x+35]=1
    mat_etat_courant[y+3,x+35]=1
    
#insert le motif du planeur
def boutonInsertPlaneur():
    global mat_etat_suivant,mat_etat_courant
    reset()
    
    x=10
    y=10
    
    mat_etat_courant[y,x]=1
    mat_etat_courant[y+1,x+1]=1
    mat_etat_courant[y+2,x+1]=1
    mat_etat_courant[y+2,x]=1
    mat_etat_courant[y+2,x-1]=1
    
    mat_etat_suivant=mat_etat_courant.copy()
    redessiner()

#reset puis affiche le canon
def boutonInsertCanon():
    global mat_etat_suivant,mat_etat_courant
    reset()
    insertCanon()
    mat_etat_suivant=mat_etat_courant.copy()
    redessiner()
    
#reset puis affiche le pentomino R
def boutonInsertPento():
    global mat_etat_suivant,mat_etat_courant
    
    reset()
    
    x=width_mat//2
    y=height_mat//2
    
    mat_etat_courant[y,x]=1
    mat_etat_courant[y,x+1]=1
    mat_etat_courant[y+1,x+1]=1
    mat_etat_courant[y+2,x+1]=1
    mat_etat_courant[y+1,x+2]=1
    
    mat_etat_suivant=mat_etat_courant.copy()
    redessiner()
    
        
    
#Fonction définissant les règles du jeu de la vie
def play(): 
    global flag, vitesse,mat_etat_courant,mat_etat_suivant,compteurCelluleActive,compteurIteration,compteurCelluleActiveTotal
    compteurIteration+=1 #On compte les itérations (pour les stats)
    
    
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            #On compte le nombre de cellules actives (pour les stats)
            if (mat_etat_courant[y,x]==1): 
                compteurCelluleActive+=1
                
            #On va compter le nombre de cellules actives entourant notre cellule
            compteurCaseActive=0 
            if (mat_etat_courant[y-1,x-1]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y-1,x]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y-1,x+1]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y,x-1]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y,x+1]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y+1,x-1]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y+1,x]==1):
                compteurCaseActive+=1
            if (mat_etat_courant[y+1,x+1]==1):
                compteurCaseActive+=1
            
            #On applique en conséquance les règles du jeu de la vie
            if (compteurCaseActive==3):
                mat_etat_suivant[y,x]=1
            elif (compteurCaseActive==2):
                mat_etat_suivant[y,x]=mat_etat_courant[y,x]
            else:
                mat_etat_suivant[y,x]=0
                
    
    compteurCelluleActiveTotal+= compteurCelluleActive
    compteurCelluleActive=0
    redessiner()
    mat_etat_courant=mat_etat_suivant.copy()
    if flag >0: 
        fen1.after(vitesse,play)

        
#fonction redessinant le tableau à partir de mat_etat
def redessiner(): 
    canDamier.delete(ALL)
    
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            w=x*c
            h=y*c
            if mat_etat_suivant[y,x]==1:
                canDamier.create_rectangle(w, h, w+c, h+c, fill='#00CD00', outline ='#00CD00')
            else:
                canDamier.create_rectangle(w, h, w+c, h+c, fill='#00CECE',outline='#00CECE')
 
    
# Programme Principal #######################################################
    

#les différentes variables:

# taille de la grille
height = 500
width = 800

#taille des cellules
c = 10

#la taille des matrices sans tenir compte de la taille des cellules
width_mat=width//c
height_mat=height//c


#vitesse de l'animation (en réalité c'est l'attente entre chaque étapes en ms)
vitesse=400



compteurIteration=0
compteurCelluleActive=0
compteurCelluleActiveTotal=0
flag=0
mat_etat_courant = np.zeros((height_mat,width_mat)) #matrice contenant le nombre de cellules vivantes autour de chaque cellule
mat_etat_suivant = np.zeros((height_mat,width_mat))  #matrice pour l'état suivant


for x in range(0,width_mat):
    for y in range(0,height_mat):
        mat_etat_courant[y,x]=0  
        mat_etat_suivant[y,x]=0

#utilisation de Tkinter
fen1 = Tk()

#titre
chaineTitre = Label(fen1)
chaineTitre.configure(text ="Simulateur du jeu de la vie\nclic gauche => cellule vivante,clic droit => cellule morte")
chaineTitre.grid(row=1,column=1,columnspan=5)

#espace du jeu de la vie
canDamier = Canvas(fen1, width =width, height =height, bg ='#EED37A')
canDamier.bind("<Button-1>", click_gauche)
canDamier.bind("<Button-3>", click_droit)
canDamier.grid(row=2,column=1,columnspan=5)


#commandes principales

bRun = Button(fen1, text ='     Go!     ', command =go)
bRun.grid(row=3,column=1,sticky='E')

bStop = Button(fen1, text ='Stop', command =stop)
bStop.grid(row=3,column=2)

bReset = Button(fen1, text ='Reset', command=reset)
bReset.grid(row=3,column=3,sticky='w')

entreeVit = Entry(fen1)
entreeVit.bind("<Return>", change_vit)
entreeVit.insert(0,vitesse)
entreeVit.grid(row=3,column=5)

chaineVit = Label(fen1)
chaineVit.configure(text = "Attente entre chaque étape (ms) :")
chaineVit.grid(row=3,column=4,sticky='E')

#Bonus

chaineDescription = Label(fen1)
chaineDescription.configure(text="\nEn hommage à John Horton Conway, l'inventeur du jeu de la vie, qui a été victime de la Covid-19 au mois d’avril dernier.\n\nVoici quelques strutctures :")
chaineDescription.grid(row=4,column=1,columnspan=5,sticky='W')

bPlaneur = Button(fen1, text="Planeur", command=boutonInsertPlaneur)
bPlaneur.grid(row=5,column=1)

chaineCanon = Label(fen1)
chaineCanon.configure(text="Le planeur est une structure se déplaçant en continu")
chaineCanon.grid(row=5,column=2,columnspan=4,sticky="W")

bCanon = Button(fen1, text ='Canon', command=boutonInsertCanon)
bCanon.grid(row=6,column=1)

chaineCanon = Label(fen1)
chaineCanon.configure(text="Ce canon a débris est le plus petit découvert à ce jour")
chaineCanon.grid(row=6,column=2,columnspan=4,sticky="W")

bPento = Button(fen1, text ='Pento', command=boutonInsertPento)
bPento.grid(row=7,column=1)

chainePento = Label(fen1)
chainePento.configure(text="Le pentomino R est un motif qui met 1103 itérations à se stabiliser !")
chainePento.grid(row=7,column=2,columnspan=4,sticky="W")


#lancement de base
exemple()
go()


fen1.mainloop()
